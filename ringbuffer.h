#ifndef __RING_BUFFER_H__
#define __RING_BUFFER_H__

enum lock_types;

struct circqueue{
    int front, rear;
    int size, capacity;
    int* arr;
    struct spinlock* spin_lock;
    struct mutex* mutex_lock;
    struct semaphore* sem_empty;
    struct semaphore* sem_full;
};

int init_ringbuffer(const int nr_slots, const enum lock_types lock_type);
void fini_ringbuffer(void);

void enqueue_ringbuffer(int value);
int dequeue_ringbuffer(void);

#endif
