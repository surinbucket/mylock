#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <sys/queue.h>

#include "config.h"
#include "locks.h"
#include "ringbuffer.h"

static int nr_slots = 0;

static enum lock_types lock_type;

struct circqueue* myqueue = NULL;

void (*enqueue_fn)(int value) = NULL;
int (*dequeue_fn)(void) = NULL;

void enqueue_ringbuffer(int value)
{
	assert(enqueue_fn);
	assert(value >= MIN_VALUE && value < MAX_VALUE);

	enqueue_fn(value);
}

int dequeue_ringbuffer(void)
{
	int value;

	assert(dequeue_fn);

	value = dequeue_fn();
	assert(value >= MIN_VALUE && value < MAX_VALUE);

	return value;
}


/*********************************************************************
 * TODO: Implement using spinlock
 */
void enqueue_using_spinlock(int value)
{
	do{
		acquire_spinlock(myqueue->spin_lock);
		if(myqueue->size != myqueue->capacity)
		{
			myqueue->rear = (myqueue->rear +1)%(myqueue->capacity);
			myqueue->arr[myqueue->rear] = value;
			myqueue->size++;
		}
		else
		{
			release_spinlock(myqueue->spin_lock);
			continue;
		}
		release_spinlock(myqueue->spin_lock);
		return;
	}while(1);
}

int dequeue_using_spinlock(void)
{
	int value = 0;
	do{
		acquire_spinlock(myqueue->spin_lock);
		if(myqueue->size != 0)
		{
			value = myqueue->arr[myqueue->front];
			myqueue->front = (myqueue->front +1) % (myqueue->capacity);
			myqueue->size--;
		}
		else
		{
			release_spinlock(myqueue->spin_lock);
			continue;
		}
		release_spinlock(myqueue->spin_lock);
		return value;
	}while(1);

	return 0;
}

void init_using_spinlock(void)
{
	enqueue_fn = &enqueue_using_spinlock;
	dequeue_fn = &dequeue_using_spinlock;
	myqueue->spin_lock = (struct spinlock*)malloc(sizeof(struct spinlock));
	init_spinlock(myqueue->spin_lock);
}

void fini_using_spinlock(void)
{
	free(myqueue->spin_lock);
}


/*********************************************************************
 * TODO: Implement using mutex
 */
void enqueue_using_mutex(int value)
{
	do{
		acquire_mutex(myqueue->mutex_lock);
		if(myqueue->size != myqueue->capacity)
		{
			myqueue->rear = (myqueue->rear +1)%(myqueue->capacity);
			myqueue->arr[myqueue->rear] = value;
			myqueue->size++;
		}
		else
		{
			release_mutex(myqueue->mutex_lock);
			continue;
		}
		release_mutex(myqueue->mutex_lock);
		return;
	}while(1);
}

int dequeue_using_mutex(void)
{
	int value = 0;
	do{
		acquire_mutex(myqueue->mutex_lock);
		if(myqueue->size != 0)
		{
			value = myqueue->arr[myqueue->front];
			myqueue->front = (myqueue->front +1) % (myqueue->capacity);
			myqueue->size--;
		}
		else
		{
			release_mutex(myqueue->mutex_lock);
			continue;
		}
		release_mutex(myqueue->mutex_lock);
		return value;
	}while(1);
	return 0;
}

void init_using_mutex(void)
{
	enqueue_fn = &enqueue_using_mutex;
	dequeue_fn = &dequeue_using_mutex;
	myqueue->mutex_lock = (struct mutex*)malloc(sizeof(struct mutex));
	init_mutex(myqueue->mutex_lock);
}

void fini_using_mutex(void)
{
	free(myqueue->mutex_lock);
}


/*********************************************************************
 * TODO: Implement using semaphore
 */
void enqueue_using_semaphore(int value)
{
	do{
		wait_semaphore(myqueue->sem_full);
		acquire_mutex(myqueue->mutex_lock);
		
		if(myqueue->size != myqueue->capacity)
		{
			myqueue->rear = (myqueue->rear +1)%(myqueue->capacity);
			myqueue->arr[myqueue->rear] = value;
			myqueue->size++;
		}
		else
		{
			release_mutex(myqueue->mutex_lock);
			continue;
		}
		
		release_mutex(myqueue->mutex_lock);
		signal_semaphore(myqueue->sem_empty);
		
		return;
	}while(1);
}

int dequeue_using_semaphore(void)
{
	int value = 0;
	do{
		wait_semaphore(myqueue->sem_empty);
		acquire_mutex(myqueue->mutex_lock);

		if(myqueue->size != 0)
		{
			value = myqueue->arr[myqueue->front];
			myqueue->front = (myqueue->front +1) % (myqueue->capacity);
			myqueue->size--;
		}
		else
		{
			release_mutex(myqueue->mutex_lock);
			continue;
		}
		release_mutex(myqueue->mutex_lock);
		signal_semaphore(myqueue->sem_full);

		return value;
	}while(1);
	return 0;
}

void init_using_semaphore(void)
{
	enqueue_fn = &enqueue_using_semaphore;
	dequeue_fn = &dequeue_using_semaphore;
	myqueue->mutex_lock = (struct mutex*)malloc(sizeof(struct mutex));
	myqueue->sem_empty = (struct semaphore*)malloc(sizeof(struct semaphore));
	myqueue->sem_full = (struct semaphore*)malloc(sizeof(struct semaphore));
	init_semaphore(myqueue->sem_empty, 0);
	init_semaphore(myqueue->sem_full, nr_slots);
	init_mutex(myqueue->mutex_lock);
}

void fini_using_semaphore(void)
{
	free(myqueue->sem_empty);
	free(myqueue->sem_full);
	free(myqueue->mutex_lock);
}

/*********************************************************************
 * Common implementation
 */
int init_ringbuffer(const int _nr_slots_, const enum lock_types _lock_type_)
{
	assert(_nr_slots_ > 0);
	nr_slots = _nr_slots_;

	/* TODO: Initialize your ringbuffer and synchronization mechanism */
	myqueue = (struct circqueue*)malloc(sizeof(struct circqueue));
	myqueue->front = 0;
	myqueue->rear = -1;
	myqueue->size = 0;
	myqueue->capacity = nr_slots;
	myqueue->arr = (int*)malloc((myqueue->capacity)*sizeof(int));

	/* Initialize lock! */
	lock_type = _lock_type_;
	switch (lock_type) {
	case lock_spinlock:
		init_using_spinlock();
		break;
	case lock_mutex:
		init_using_mutex();
		break;
	case lock_semaphore:
		init_using_semaphore();
		break;
	}
	return 0;
}

void fini_ringbuffer(void)
{
	/* TODO: Clean up what you allocated */
	switch (lock_type) {
		case lock_spinlock:
			fini_using_spinlock();
			break;
		case lock_mutex:
			fini_using_mutex();
			break;
		case lock_semaphore:
			fini_using_semaphore();
			break;
		default:
			break;
	}
	free(myqueue->arr);
	free(myqueue);
}
