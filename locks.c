#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/queue.h>

#include "config.h"
#include "locks.h"
#include "atomic.h"

/******************************************************************
 * Spinlock implementation
 */
void init_spinlock(struct spinlock *lock)
{
	lock->locked = 0;
	return;
}

void acquire_spinlock(struct spinlock *lock)
{
	while(compare_and_swap(&lock->locked, 0, 1));
	return;
}

void release_spinlock(struct spinlock *lock)
{
	compare_and_swap(&lock->locked, 1, 0);
	return;
}


/******************************************************************
 * Blocking lock implementation
 *
 * Hint: Use pthread_self, pthread_kill, pause, and signal
 */
static int flag = 0;
void sig_handler(int signo)
{
	flag = 1;
	return;
}

void init_mutex(struct mutex *lock)
{
	lock->acquired = 0;
	init_spinlock(&lock->spin_lock);
	TAILQ_INIT(&lock->waiters);
	return;
}

void acquire_mutex(struct mutex *lock)
{
	struct thread* t;
	t = (struct thread*)malloc(sizeof(struct thread));
	signal(SIGUSR1,sig_handler);
	do{
		acquire_spinlock(&lock->spin_lock);

		if(lock->acquired == 0)
		{
			lock->acquired = 1;
			release_spinlock(&lock->spin_lock);
			break;
		}
		t->pthread = pthread_self();
		TAILQ_INSERT_TAIL(&lock->waiters, t, next);
		release_spinlock(&lock->spin_lock);
		pause();
	}while(1);
}

void release_mutex(struct mutex *lock)
{
	acquire_spinlock(&lock->spin_lock);
	if(TAILQ_EMPTY(&lock->waiters))
	{
		lock->acquired = 0;
		release_spinlock(&lock->spin_lock);
		return;
	}
	struct thread* t = lock->waiters.tqh_first;
	TAILQ_REMOVE(&lock->waiters, t, next);
	pthread_kill(t->pthread, SIGUSR1);

	while(!flag){
		usleep(10);
		pthread_kill(t->pthread, SIGUSR1);
	}
	free(t);
	lock->acquired = 0;
	release_spinlock(&lock->spin_lock);
	return;
}


/******************************************************************
 * Semaphore implementation
 *
 * Hint: Use pthread_self, pthread_kill, pause, and signal
 */
void sig_handler_sem(int signo)
{
	flag = 1;
	return;
}

void init_semaphore(struct semaphore *sem, int S)
{
	sem->value = S;
	init_spinlock(&sem->spinlock);
	TAILQ_INIT(&sem->waiters);
	return;
}

void wait_semaphore(struct semaphore *sem)
{
	struct thread* t;
	t = (struct thread*)malloc(sizeof(struct thread));
	signal(SIGUSR1, sig_handler_sem);

	acquire_spinlock(&sem->spinlock);
	sem->value;
	release_spinlock(&sem->spinlock);

	do{
		if(&sem->spinlock.locked == 0)
			acquire_spinlock(&sem->spinlock);

		if(sem->value < 0){
			t->pthread = pthread_self();
			TAILQ_INSERT_TAIL(&sem->waiters, t, next);
			release_spinlock(&sem->spinlock);
			pause();
		}
		else
		{
			release_spinlock(&sem->spinlock);
			return;
		}
	}while(1);
	return;
}

void signal_semaphore(struct semaphore *sem)
{
	acquire_spinlock(&sem->spinlock);
	sem->value++;

	if(sem->value <= 0)
	{
		struct thread* t = sem->waiters.tqh_first;

		TAILQ_REMOVE(&sem->waiters, t, next);
		pthread_kill(t->pthread, SIGUSR1);

		while(!flag)
		{
			usleep(20);
			pthread_kill(t->pthread, SIGUSR1);
		}
		free(t);
		release_spinlock(&sem->spinlock);
	}
	else
	{
		release_spinlock(&sem->spinlock);
	}
	return;
}


/******************************************************************
 * Spinlock tester exmaple
 */
struct spinlock testlock;
struct mutex testm;

int testlock_held = 0;

void *test_thread(void *_arg_)
{
	usleep(random() % 1000 * 1000);

	printf("Tester acquiring the lock...\n");
	//acquire_spinlock(&testlock);
	acquire_mutex(&testm);

	printf("Tester acquired\n");
	assert(testlock_held == 0);
	testlock_held = 1;

	sleep(1);

	printf("Tester releases the lock\n");
	testlock_held = 0;
	//release_spinlock(&testlock);
	release_mutex(&testm);

	printf("Tester released the lock\n");
	return 0;
}

void test_lock(void)
{
	/* Set nr_testers as you need
	 *  1: one main, one tester. easy :-)
	 * 16: one main, 16 testers contending the lock :-$
	 */
	const int nr_testers = 16;
	//const int nr_testers = 1;
	int i;
	pthread_t tester[nr_testers];

	printf("Main initializes the lock\n");
	//init_spinlock(&testlock);
	init_mutex(&testm);

	printf("Main graps the lock...");
	//acquire_spinlock(&testlock);
	acquire_mutex(&testm);
	
	assert(testlock_held == 0);
	testlock_held = 1;
	printf("acquired!\n");

	for (i = 0; i < nr_testers; i++) {
		pthread_create(tester + i, NULL, test_thread, NULL);
	}

	sleep(1);

	printf("Main releases the lock\n");
	testlock_held = 0;
	//release_spinlock(&testlock);
	release_mutex(&testm);

	printf("Main released the lock\n");

	for (i = 0; i < nr_testers; i++) {
		pthread_join(tester[i], NULL);
	}
	assert(testlock_held == 0);
	//printf("Your spinlock implementation looks O.K.\n");
	printf("Your mutex implementation looks O.K.\n");
	
	return;
}

